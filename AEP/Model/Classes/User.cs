﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP
{
    class User
    {
        public User(int pin, string email, string psw)
        {
            Pin = pin;
            Email = email;
            Psw = psw;
        }
        
        public int Pin { set; get; }
        public string Email { set; get; }
        public string Psw { set; get; }
    }
}
