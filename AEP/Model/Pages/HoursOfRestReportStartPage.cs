﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;

namespace AEP.Model.Pages
{
    class HoursOfRestReportStartPage
    {
        public HoursOfRestReportStartPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "html/body/span/div/div/div[2]/div/select")]
        public IWebElement ddlSelectPeriod { get; set; }

        public List<IWebElement> crewMembers { get; set; }
        

        public void SelectAllMembers()
        {
            for (int i = 0; i < crewMembers.Count; i++)
            {
                crewMembers[i].Click();
                //PropertiesCollection.driver.WaitForLoad(); - it used for fast report
            }
        }
    }
}
