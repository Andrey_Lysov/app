﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP.ConvertToDataTable
{
    public class HtmlTableToDataTable
    {
        List<DataCollection> _dataTable = new List<DataCollection>();

        public void ReadTable(IWebElement table)
        {
            //Get columns from the table
            var columns = table.FindElements(By.TagName("th"));

            //Get rows from the table
            var rows = table.FindElements(By.TagName("tr"));

            //Create row index
            int rowIndex = 0;

            foreach(var row in rows)
            {
                int colIndex = 0;

                var colDatas = row.FindElements(By.TagName("td"));

                foreach(var columnValue in colDatas)
                {
                    _dataTable.Add(new DataCollection
                    {
                        rowNumber = rowIndex,
                        colName = columns[colIndex].Text,
                        colValue = columnValue.Text
                    });

                    //Move to next column
                    colIndex++;
                }
                //Move to the next row
                rowIndex++;
            }
        }

        public string ReadCell(int rowNum, string columnName)
        {
            var data = (from table in _dataTable
                        where table.colName == columnName && table.rowNumber == rowNum
                        select table.colValue).SingleOrDefault();
            return data;
        }
    }
}
