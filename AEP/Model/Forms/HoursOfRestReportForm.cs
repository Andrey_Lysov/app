﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.ObjectModel;
using System.Threading;

namespace AEP.Model.Forms
{
    class HoursOfRestReportForm
    {
        public HoursOfRestReportForm()
        {
            while (!waitUntilPageElementIsDisplayed())
            {
                PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(300));
            }
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "taa-periods")]
        public IWebElement ddlPeriods { get; set; }

        [FindsBy(How = How.Id, Using = "taa-pins")]
        public IWebElement ddlPins { get; set; }

        [FindsBy(How = How.Id, Using = "submit")]
        public IWebElement btnSubmit { get; set; }

        [FindsBy(How = How.Id, Using = "cancel")]
        public IWebElement btnCancel { get; set; }

        public ReadOnlyCollection<IWebElement> getAllPeriods()
        {
            return ddlPeriods.getAllItems
                (Locator.xpath, "//body/div[7]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div");
        }

        public void SelectPeriod(IWebElement dropDownList, IWebElement period)
        {
            dropDownList.SelectDropDownActions(period);
        }

        public void SelectAllPins()
        {
            while (!SeleniumGetMethods.waitUntilElementIsEnabled(ddlPins))
            {
                PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(300));
            }
            PropertiesCollection.action.MoveToElement(ddlPins).Click().Build().Perform();
            /*
            while (!SeleniumGetMethods.waitUntilElementIsEnabledBySelector(Locator.classname, "dx-list-select-all-label"))
            {
                PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(300));
            }
            */
            var allPinsItem = PropertiesCollection.driver.FindElement(By.ClassName("dx-list-select-all-label"), 10);
            PropertiesCollection.action.MoveToElement(allPinsItem).Click().Build().Perform();
            PropertiesCollection.driver.WaitForLoad();
        }


        public bool waitUntilPageElementIsDisplayed()
        {
            try
            {
                var element1 = PropertiesCollection.driver.FindElement(By.Id("taa-periods"));
                var element2 = PropertiesCollection.driver.FindElement(By.Id("taa-pins"));
                var element3 = PropertiesCollection.driver.FindElement(By.Id("submit"));
                var element4 = PropertiesCollection.driver.FindElement(By.Id("cancel"));
                return element1.Enabled && element2.Enabled && element3.Enabled && element4.Enabled;
            }
            catch
            {
                return false;
            }
        }
    }
}