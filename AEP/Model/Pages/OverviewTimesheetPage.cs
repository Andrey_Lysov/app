﻿using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP.Model.Pages
{
    class OverviewTimesheetPage : MyTimesheetsPage
    {
        public OverviewTimesheetPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
    }
}
