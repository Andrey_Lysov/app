﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AEP.Model.Pages
{
    class MyTimesheetsPage
    {
        public MyTimesheetsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "miTimeSheetMenu_DXI0_T")]
        public IWebElement btnCurrent { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetMenu_DXI1_IS")]
        public IWebElement btnOverview { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetMenu_DXI2_T")]
        public IWebElement btnPastDueAndRejected { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetMenu_DXI3_T")]
        public IWebElement btnSubmitted { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetMenu_DXI4_T")]
        public IWebElement ddlReport { get; set; }

        [FindsBy(How = How.Id, Using = "logOffLink")]
        public IWebElement btnLogOff { get; set; }

        public LoginPage LogOff()
        {
            btnLogOff.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new LoginPage();
        }
    }
}