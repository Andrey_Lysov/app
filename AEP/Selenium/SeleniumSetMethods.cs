﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AEP
{
    static class SeleniumSetMethods
    {
        public static void EnterTextActions(this IWebElement element, string value)
        {
            PropertiesCollection.action.MoveToElement(element);
            PropertiesCollection.action.Click();
            PropertiesCollection.action.SendKeys(value);
            PropertiesCollection.action.Build().Perform();
        }


        /// <summary>
        /// Extended method for entering text in the control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void EnterText(this IWebElement element, String value)
        {
            element.Click();
            try {
                element.SendKeys(value);
            }
            catch (Exception e)
            {
                string err = e.InnerException.Message;
            }

        }

        /// <summary>
        /// Select a drop down control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SelectDropDown(this IWebElement element, String value)
        {
            new SelectElement(element).SelectByText(value);
        }

        public static void SelectDropDownActions(this IWebElement element, IWebElement item)
        {
            PropertiesCollection.action.MoveToElement(item);
            PropertiesCollection.action.Click();
            PropertiesCollection.action.Build().Perform();
        }

        /// <summary>
        /// Method for downloading all items of a dynamic dropdown list
        /// (NOT IMPLEMENTED)
        /// </summary>
        /// <param name="elementText"></param>
        public static void ScrollUntilElementIsPresent(IWebElement elementText)
        {
            try
            {
                IWebElement element = PropertiesCollection.driver.FindElement(By.XPath("//*[text()='" + elementText + "']"));
            }
            catch (Exception)
            {
                PropertiesCollection.jse.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
                ScrollUntilElementIsPresent(elementText);
            }
        }

        public static void SelectDropDownItem(this IWebElement element, string value)
        {
            PropertiesCollection.action.MoveToElement(element);
            PropertiesCollection.action.Click();
            PropertiesCollection.action.Build().Perform();
            IWebElement item = PropertiesCollection.driver.FindElement(By.XPath("//*[text()='" + value + "']"));
            PropertiesCollection.action.MoveToElement(item).Click();
            PropertiesCollection.action.Build().Perform();
        }
    }
}
