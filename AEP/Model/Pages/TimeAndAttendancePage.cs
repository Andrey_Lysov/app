﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AEP.Model.Pages
{
    class TimeAndAttendancePage
    {
        public TimeAndAttendancePage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "divTileMyTimesheet")]
        public IWebElement tileMyTimesheets { get; set; }

        [FindsBy(How = How.Id, Using = "divTileCoWorkersTimesheet")]
        public IWebElement tileCoWorkersTimesheets { get; set; }

        [FindsBy(How = How.Id, Using = "divTileMessage")]
        public IWebElement tileMessages { get; set; }

        [FindsBy(How = How.Id, Using = "logOffLink")]
        public IWebElement btnLogOff { get; set; }

        public MyTimesheetsPage NavigateToMyTimesheets()
        {
            tileMyTimesheets.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new MyTimesheetsPage();
        }

        public CoWorkersTimesheetsPage NavigateToCoWorkersTimesheets()
        {
            tileCoWorkersTimesheets.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new CoWorkersTimesheetsPage();
        }

        public TAAMessages NavigateToMessages()
        {
            tileMessages.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new TAAMessages();
        }

        public LoginPage LogOff()
        {
            btnLogOff.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new LoginPage();
        }
    }
}
