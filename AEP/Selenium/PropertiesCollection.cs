﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace AEP
{
    public static class PropertiesCollection
    {
        //Auto-implemented Property
        public static IWebDriver driver { get; set; }
        public static WebDriverWait wait { get; set; }
        public static Actions action { get; set; }
        public static PopupWindowFinder finder { get; set; }
        public static IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;

        public static void WaitForLoad(this IWebDriver jse)
        {
            wait.Until((driver => Convert.ToString(((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState")) == "complete"));
        }
    }
}
