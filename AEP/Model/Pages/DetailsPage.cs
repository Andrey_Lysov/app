﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP.Model.Pages
{

    class DetailsPage : MyPersonalDetailsPage
    {
        public DetailsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
        /*
         * Information for elements which define by XPath
         * 1-st number define row number 2-nd number define column
         * of the field on the "Personal Details" form
         * (valid numbers for 1-st number 1-9, for 2-nd - 1,2)
         * XPath for Pin field - html/body/div/div/div/div/form/div/div/div/div[1]/div/div[1]/div/div/div/div/div
         */

        [FindsBy(How = How.Id, Using = "html/body/div/div/div/div/form/div/div/div/div[1]/div/div[1]/div/div/div/div/div")]
        public IWebElement intPinDisabled { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[1]/div/div[2]/div/div/div/div")]
        public IWebElement ddlTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[2]/div/div[1]/div/div/div/div/div")]
        public IWebElement txtFirstName { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[2]/div/div[2]/div/div/div/div/div")]
        public IWebElement txtLastName { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[3]/div/div[1]/div/div/div/div/div")]
        public IWebElement txtMiddleName { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[3]/div/div[2]/div/div/div/div/div")]
        public IWebElement txtCallingName { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[4]/div/div[1]/div/div/div/div/div")]
        public IWebElement txtInitials { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[4]/div/div[2]/div/div/div/div/div")]
        public IWebElement ddlSuffixName { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[5]/div/div[1]/div/div/div/div/div")]
        public IWebElement ddlNationalities { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[5]/div/div[2]/div/div/div/div/div")]
        public IWebElement txtPlaceOfBirth { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[6]/div/div[1]/div/div/div/div/div")]
        public IWebElement clndrBirthDate { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[6]/div/div[2]/div/div/div/div/div")]
        public IWebElement intAgeDisabled { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[7]/div/div[1]/div/div/div/div/div")]
        public IWebElement ddlCountryOfBirth { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[7]/div/div[2]/div/div/div/div/div")]
        public IWebElement ddlMaritalStatus { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[8]/div/div[1]/div/div/div/div/div")]
        public IWebElement ddlHomeAirport { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[8]/div/div[2]/div/div/div/div/div")]
        public IWebElement intTravelTime { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[9]/div/div[1]/div/div/div/div/div")]
        public IWebElement ddlHomeAirport2 { get; set; }

        [FindsBy(How = How.XPath, Using = "html/body/div/div/div/div/form/div/div/div/div[9]/div/div[2]/div/div/div/div/div")]
        public IWebElement intTravelTime2 { get; set; }


    }
}
