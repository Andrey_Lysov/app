﻿using OpenQA.Selenium.Support.PageObjects;

namespace AEP.Model.Pages
{
    class SubmittedTimesheetsPage : MyTimesheetsPage
    {
        public SubmittedTimesheetsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
    }
}