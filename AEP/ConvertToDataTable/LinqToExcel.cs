﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP
{
    class LinqToExcel
    {
        public static User getUserByPin(int pin, string credentialsTable = "CredentialsByPIN.xlsx")
        {
            string connectionString = @"C:\Users\Andrey.Lysov\Desktop\Local Repository\AEP\AEP\" + credentialsTable;
            string sheetName = "Sheet1";
            var excelFile = new ExcelQueryFactory(connectionString);

            var credentials = from a in excelFile.Worksheet(sheetName)
                              where a["PIN"] == Convert.ToString(pin)
                              select a;

            return new User(Convert.ToInt16(credentials.FirstOrDefault()[0].Value), Convert.ToString(credentials.FirstOrDefault()[1].Value), Convert.ToString(credentials.FirstOrDefault()[2].Value));
        }
    }
}