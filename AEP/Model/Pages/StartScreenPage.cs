﻿using AEP.Model.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AEP
{
    class StartScreenPage
    {
        public StartScreenPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "divTilePersonalDetails")]
        public IWebElement tileMyPersonalDetails { get; set; }

        [FindsBy(How = How.Id, Using = "divTileFlightDetails")]
        public IWebElement tileMyFlight { get; set; }

        [FindsBy(How = How.Id, Using = "divTileDayToDay")]
        public IWebElement tileDayToDay { get; set; }

        [FindsBy(How = How.Id, Using = "divTileProjectTimesheet")]
        public IWebElement tileProjectTimesheet { get; set; }

        [FindsBy(How = How.Id, Using = "divTileTimeAndAttendance")]
        public IWebElement tileTimeAndAttandance { get; set; }

        [FindsBy(How = How.Id, Using = "divTileTravelBill")]
        public IWebElement tileTravelExpense { get; set; }

        [FindsBy(How = How.Id, Using = "divTileWorkDaysAccounting")]
        public IWebElement tileWDAAndExpenses { get; set; }

        [FindsBy(How = How.Id, Using = "logOffLink")]
        public IWebElement btnLogOff { get; set; }

        public DetailsPage NavigateToMyPersonalDetails()
        {
            tileMyPersonalDetails.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new DetailsPage();
        }

        public MyTimesheetsPage NavigateToMyFlight()
        {
            tileMyFlight.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new MyTimesheetsPage();
        }

        public DayToDayPlanningPage NavigateToDayToDayPlanning()
        {
            tileDayToDay.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new DayToDayPlanningPage();
        }

        public ProjectTimesheetPage NavigateToProjectTimesheet()
        {
            tileProjectTimesheet.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new ProjectTimesheetPage();
        }

        public TimeAndAttendancePage NavigateToTimeAndAttendance()
        {
            tileTimeAndAttandance.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new TimeAndAttendancePage();
        }

        public TravelExpensePage NavigateToTravelExpense()
        {
            tileTravelExpense.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new TravelExpensePage();
        }

        public WDAPage NavigateToWDA()
        {
            tileWDAAndExpenses.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new WDAPage();
        }

        public LoginPage LogOff()
        {
            btnLogOff.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new LoginPage();
        }
    }
}
