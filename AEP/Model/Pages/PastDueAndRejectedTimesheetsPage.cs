﻿using OpenQA.Selenium.Support.PageObjects;

namespace AEP.Model.Pages
{
    class PastDueAndRejectedTimesheetsPage : MyTimesheetsPage
    {
        public PastDueAndRejectedTimesheetsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
    }
}