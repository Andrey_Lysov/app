﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace AEP.Model.Pages
{
    class CurrentTimesheetPage : MyTimesheetsPage
    {
        public CurrentTimesheetPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "cbDayCount_I")]
        public IWebElement ddlDayCount { get; set; }

    }


}
