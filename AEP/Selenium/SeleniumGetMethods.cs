﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace AEP
{
    public enum Locator{
        xpath,
        id,
        classname
    }

    public static class SeleniumGetMethods
    {
        public static string GetText(IWebElement element)
        {
            return element.GetAttribute("value");
        }

        public static string GetTextFromDDL(IWebElement element)
        {
            return new SelectElement(element).AllSelectedOptions.SingleOrDefault().Text;
        }

        public static ReadOnlyCollection<IWebElement> getAllItems(this IWebElement dropDownList, Locator locator, string indicator, int? index = null)
        {
            if (index == null)
                index = 0;
            try
            {
                dropDownList.Click();
            }
            catch (TargetInvocationException)
            {
                PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
                if (index == 10)
                    throw new Exception("Hi-hi");

                index++;
                getAllItems(dropDownList, locator, indicator, index);
            }
            while (!waitUntilElementIsDisplayed(locator, indicator))
            {
                PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(500));
            } 
            PropertiesCollection.driver.WaitForLoad();
            return PropertiesCollection.driver.FindElements(By.XPath(indicator));
        }

        public static bool waitUntilElementIsDisplayed(Locator locator,string path)
        {
            IWebElement element;
            try
            {
                switch (locator)
                {
                    case Locator.xpath:
                        element = PropertiesCollection.driver.FindElement(By.XPath(path));
                        break;
                    case Locator.classname:
                        element = PropertiesCollection.driver.FindElement(By.ClassName(path));
                        break;
                    case Locator.id:
                        element = PropertiesCollection.driver.FindElement(By.Id(path));
                        break;
                    default:
                        return false;
                }
                return element.Displayed;
            }
            catch
            {
                return false;
            }   
        }

        public static bool waitUntilElementIsEnabledBySelector(Locator locator, string indicator)
        {
            IWebElement element;
            switch (locator)
            {
                case Locator.xpath:
                    element = PropertiesCollection.driver.FindElement(By.XPath(indicator));
                    break;
                case Locator.classname:
                    element = PropertiesCollection.driver.FindElement(By.ClassName(indicator));
                    break;
                case Locator.id:
                    element = PropertiesCollection.driver.FindElement(By.Id(indicator));
                    break;
                default:
                    throw new NoSuchElementException("Element was not found");
            }
            return waitUntilElementIsEnabled(element);
        }

        public static bool waitUntilElementIsEnabled(IWebElement element)
        {
            try
            {
                return element.Enabled;
            }
            catch
            {
                return false;
            }
        }

        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }
    }
}
