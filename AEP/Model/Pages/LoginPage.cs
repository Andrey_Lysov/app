﻿using AEP;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace AEP
{
    
    class LoginPage
    {
        public LoginPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "tbPin_I")]
        public IWebElement txtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "tbPasscode_I")]
        public IWebElement txtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement btnLogin { get; set; }

        public StartScreenPage LoginPin(int pin)
        {

            User user = LinqToExcel.getUserByPin(10);

            txtEmail.EnterText(Convert.ToString(user.Pin));
            txtPassword.EnterText(user.Psw);
            PropertiesCollection.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(500));
            btnLogin.Click();

            Console.WriteLine("user is logined with credentials username " + user.Email + " and password " + user.Psw);
            PropertiesCollection.driver.WaitForLoad();
            return new StartScreenPage();

        }
    }
}