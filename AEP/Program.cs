﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using AEP.Model.Pages;
using OpenQA.Selenium.Interactions;
using AEP.Model.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AEP
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        [SetUp]
        public void Initialize()
        {
            PropertiesCollection.driver = new ChromeDriver();
            PropertiesCollection.action = new Actions(PropertiesCollection.driver);
            PropertiesCollection.finder = new PopupWindowFinder(PropertiesCollection.driver);
            PropertiesCollection.wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(5));

        //Navigate to (for C# course - http://executeautomation.com/demosite/Login.html, for secondary course - https://www.google.com)
            PropertiesCollection.driver.Navigate().GoToUrl("http://192.168.1.202/APP/adoniskh/crew");
            Console.WriteLine("Opened Url");
        }

        [Test]
        public void LoginTest()
        {
            LoginPage loginPage = new LoginPage();
            StartScreenPage startPage = loginPage.LoginPin(10);
            TimeAndAttendancePage taa = startPage.NavigateToTimeAndAttendance();
            CoWorkersTimesheetsPage cwp = taa.NavigateToCoWorkersTimesheets();
            string currentHandle = PropertiesCollection.driver.CurrentWindowHandle;
            HoursOfRestReportForm reportForm = cwp.MoveHoursOfRestReport();
            var periods = reportForm.getAllPeriods();

            foreach(IWebElement period in periods)
            {
                reportForm.ddlPeriods.SelectDropDownActions(period);
                reportForm.SelectAllPins();
                reportForm.btnSubmit.Submit();
            }


            //PropertiesCollection.driver.SwitchTo().Window();
            //hor.crewMembers = hor.ddlSelectPeriod.SelectPeriod("201612");
            //hor.SelectAllMembers();
        }
        
        [TearDown]
        public void Cleaner()
        {
            PropertiesCollection.driver.Quit();
            Console.WriteLine("Closed the browser");
        }
    }
}