﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEP.Model.Pages
{
    class MyPersonalDetailsPage
    {
        public MyPersonalDetailsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI0_T")]
        public IWebElement btnDetails { get; set; }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI1_T")]
        public IWebElement btnAddress { get; set; }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI2_T")]
        public IWebElement btnNextOfKin { get; set; }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI3_T")]
        public IWebElement btnTravelDocuments { get; set; }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI4_T")]
        public IWebElement btnCertificates { get; set; }

        [FindsBy(How = How.Id, Using = "PDMenu_DXI5_T")]
        public IWebElement btnMedical { get; set; }

        [FindsBy(How = How.Id, Using = "logOffLink")]
        public IWebElement btnLogOff { get; set; }

        public void SelectDetails()
        {
            btnDetails.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public void SelectAddress()
        {
            btnAddress.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public void SelectNextOfKin()
        {
            btnNextOfKin.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public void SelectTravelDocuments()
        {
            btnTravelDocuments.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public void SelectCertificates()
        {
            btnCertificates.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public void SelectMedical()
        {
            btnMedical.Click();
            PropertiesCollection.driver.WaitForLoad();
        }

        public LoginPage LogOff()
        {
            btnLogOff.Click();
            PropertiesCollection.driver.WaitForLoad();
            return new LoginPage();
        }
    }
}
