﻿using AEP.Model.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace AEP.Model.Pages
{
    class CoWorkersTimesheetsPage
    {
        public CoWorkersTimesheetsPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI0_T")]
        public IWebElement ddlDayToDay { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI1_T")]
        public IWebElement ddlNonconformingEvents { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI2_T")]
        public IWebElement ddlMyTeam { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI3_T")]
        public IWebElement ddlDocumentsWorkflow { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI4_T")]
        public IWebElement ddlReports { get; set; }

        [FindsBy(How = How.Id, Using = "miTimeSheetCoworkersMenu_DXI5_T")]
        public IWebElement btnSendMessages { get; set; }

        public HoursOfRestReportForm MoveHoursOfRestReport(string item = "Generate", string secondItem = "DevEx Work and Rest")
        {
            PropertiesCollection.action.MoveToElement(ddlReports);
            PropertiesCollection.action.Click().Build().Perform();
            PropertiesCollection.action.MoveToElement(PropertiesCollection.driver.FindElement(By.XPath("//*[text()='" + item + "']")));
            PropertiesCollection.action.Click().Build().Perform();
            PropertiesCollection.action.MoveToElement(PropertiesCollection.driver.FindElement(By.XPath("//*[text()='" + secondItem + "']")));
            PropertiesCollection.action.Click().Build().Perform();
            PropertiesCollection.wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("taa-periods")));
            return new HoursOfRestReportForm();
        }
    }
}
